Feature: Search for the product

  @Api, @Positive
  Scenario Outline: Searching for the product by product name
    When the user calls the endpoint "<Product>"
    Then the user should receive a response with status code <Status Code>
    Then the user should see the results displayed for "<Product>" in all titles
    Examples:
      | Product | Status Code |
      | cola    | 200         |

  @Api, @Negative
  Scenario Outline: Searching for the product by non-existed name
    When the user calls the endpoint "<Product>"
    Then the user should receive a response with status code <Status Code>
    Then the user should see the "Not found" message
    Examples:
      | Product | Status Code |
      | test    | 404         |