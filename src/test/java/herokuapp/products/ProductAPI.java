package herokuapp.products;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.environment.SystemEnvironmentVariables;

public class ProductAPI {
    @Step
    public void searchByProductName(String name) {
        SerenityRest
                .given()
                .baseUri(SystemEnvironmentVariables.createEnvironmentVariables().getProperty("environments.default.baseUrl"))
                .basePath(SystemEnvironmentVariables.createEnvironmentVariables().getProperty("environments.default.searchEndpoint"))
                .get(name);
    }
}
