package herokuapp.products;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;

import java.util.List;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class ProductStepDefinitions {
    @Steps
    ProductAPI productAPI;

    @When("the user calls the endpoint {string}")
    public void theUserCallsTheEndpoint(String product) {
        productAPI.searchByProductName(product);
    }

    @Then("the user should receive a response with status code {int}")
    public void theUserShouldReceiveAResponseWithStatusCode(int code) {
        restAssuredThat(response -> response.statusCode(code));
    }

    @Then("the user should see the results displayed for {string} in all titles")
    public void theUserShouldSeeTheResultsDisplayedForInAllTitles(String expectedResult) {
        List<String> listOfTitles = lastResponse().body().jsonPath().get("title");
        Assertions.assertThat(listOfTitles)
                .allMatch(item -> item.toLowerCase().contains(expectedResult));
    }

    @Then("the user should see the {string} message")
    public void theUserShouldSeeTheMessage(String expectedMessage) {
        String message = lastResponse().body().jsonPath().get("detail.message");
        Assertions.assertThat(message).isEqualTo(expectedMessage);
    }
}
