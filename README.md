# QA Assignment

## The repository contains automation framework for waarkoop-server herokuapp

Requirement:
- Java 16
- Maven 3.8.1

*Kye libraries uses:*
- Junit5
- Assertj
- Serenity-Rest

### Refactoring summary
1. Deleted unused code
2. Separate API client (ProductApi.class) and StepDefinition
3. Use richer assertion library
4. Env variables were moved to .conf file
5. Update dependencies

### Setup
1. Clone projects
```
  > git clone 
```

### Test Execution
Run the tests with
```
  > maven verify
```

### Test Summary Report
Report will be generated with ```verify``` command and link will be provided inside console
To generete new report run
```
  > maven serenity:aggregate
```
